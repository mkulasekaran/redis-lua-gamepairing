--local K=32
local K = ARGV[#ARGV]
local count=0
local total=0
local scoreTable={}
local scoreboard=KEYS[1].."_scoreboard"
 
local function elo(Avgvalue,Value)
    return (1/(1+math.pow(10,((Avgvalue-Value)/400))))
end
      
local function round(num) 
  if num >= 0 then return math.floor(num+.5) 
  else return math.ceil(num-.5) end
end

for nameCount = 1, #ARGV-1 do
      scoreTable[ARGV[nameCount]]=redis.call("ZSCORE", scoreboard, ARGV[nameCount])
      total=total+tonumber(scoreTable[ARGV[nameCount]])
      count=count+1
end
 
local avg=total/count
count=count-1
for nameCount = 1, #ARGV-1 do
    local Zscore=scoreTable[ARGV[nameCount]]
    local countValue=(1-((nameCount-1)/count))
    local Ratingscore=round(K*(countValue-elo(avg,Zscore)))
    redis.call("ZINCRBY", scoreboard,Ratingscore,ARGV[nameCount])
end
