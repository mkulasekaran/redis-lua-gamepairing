local redis = require 'redis'
local dumpLib = require 'dump'

local dump = function(v) 
    if nil ~= v then
        print(dumpLib.tostring(v))
    end
end

-- If you have some different host/port change it here
local host = "127.0.0.1"
local port = 6379

client = redis.connect(host, port)

-- Workaround for absence of redis.call or atelast I did not find one
-- And did not want to digg in redis source code to see how does he get redis.call
redis.call = function(cmd, ...) 
    return assert(loadstring('return client:'.. string.lower(cmd) ..'(...)'))(...)
end

local f = function (KEYS, ARGV)
        
        local errorMessage=false
        local loop=ARGV[2]-1
        
       -- if ( #ARGV == 3) then
        local userscore=redis.call("ZSCORE",KEYS[1].."_scoreboard",ARGV[1])
        redis.call("ZADD",KEYS[1],userscore,ARGV[1])
        --end  
        
        local size=redis.call("ZCARD",KEYS[1])
        
        if (size < tonumber(ARGV[2])) then
          return "No Match available"
        end          
                
        local rank=redis.call("ZRANK",KEYS[1],ARGV[1])
        local lowerbound=rank
        local upperbound=rank
        
        local function round(num) 
          if num >= 0 then return math.floor(num+.5) 
          else return math.ceil(num-.5) end
        end
        
        local function updateUpperBound()
          local Value=upperbound+1
          if (Value <= size) then
            upperbound=Value
          elseif (lowerbound > 0) then  
            lowerbound = lowerbound -1
          else
            errorMessage=true
          end  
        end  
        
        local function updateLowerBound()
          local Value=lowerbound-1
          if (Value >= 0) then
            lowerbound=Value
          elseif (upperbound < size) then  
            upperbound=upperbound+1
          else
            errorMessage=true
          end  
        end  
        
        for counter = 1, loop do
          if(round(math.random()) == 1) then
             updateUpperBound()
          else
             updateLowerBound()
          end   
        end 
        
        if (errorMessage) then
          return "No Match available"
        end  
        
        local result=redis.call("ZRANGE",KEYS[1],lowerbound,upperbound,"WITHSCORES")
        redis.call("ZREMRANGEBYRANK",KEYS[1],lowerbound,upperbound)
        return result
end
--          if (ARGV[nameCount] == "win" or ARGV[nameCount] == "draw" or ARGV[nameCount] == "lose" ) then            

-- Populate your ARGV and KEYS variables
--local ARGV = {"win:player1,player2","draw:player4","lose:player3"}
local KEYS = {"set"}
local ARGV = {"player15","2"}

-- If you need to use some other DB uncomment next line and change number
-- redis.call("SELECT", 1)

dump(f(KEYS, ARGV))