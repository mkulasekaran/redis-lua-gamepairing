local redis = require 'redis'
local dumpLib = require 'dump'

local dump = function(v) 
    if nil ~= v then
        print(dumpLib.tostring(v))
    end
end

-- If you have some different host/port change it here
local host = "127.0.0.1"
local port = 6379

client = redis.connect(host, port)

-- Workaround for absence of redis.call or atelast I did not find one
-- And did not want to digg in redis source code to see how does he get redis.call
redis.call = function(cmd, ...) 
    return assert(loadstring('return client:'.. string.lower(cmd) ..'(...)'))(...)
end

local f = function (KEYS, ARGV)
        
        local keyLookup=KEYS[1]..ARGV[3]
        
        local size=redis.call("ZCARD",keyLookup)
        
        if (size < 1) then
          return "No Match available"
        end          

        local userlookup=KEYS[1]..ARGV[1]
        local result={}
        local rangeValue={}
        local defaultratings=1000
        local userscore=redis.call("ZSCORE",KEYS[1].."_scoreboard",ARGV[2])
        
        userscore=true and userscore or defaultratings
        
        for counter = 1,ARGV[5] do
            local interval=counter*ARGV[4]
            rangeValue=redis.call("ZRANGEBYSCORE",keyLookup,userscore-interval,userscore+interval)
            if table.getn(rangeValue) > 0  then 
              break 
            end
        end
        
        if (table.getn(rangeValue) == 0) then 
            rangeValue=redis.call("ZRANGEBYSCORE",keyLookup,"-inf","+inf")
            if (table.getn(rangeValue)==0) then
              return  "No Match available"
            end  
        end  
        
        local tablesize=table.getn(rangeValue)
        local userFound=rangeValue[math.ceil(tablesize/2)]
        
        redis.call("ZREM",keyLookup,userFound)
        redis.call("ZREM",userlookup,ARGV[2])
        table.insert(result, userFound) 
        table.insert(result, ARGV[2])  

        return result
end
--          if (ARGV[nameCount] == "win" or ARGV[nameCount] == "draw" or ARGV[nameCount] == "lose" ) then            

-- Populate your ARGV and KEYS variables
--local ARGV = {"win:player1,player2","draw:player4","lose:player3"}
local KEYS = {"chess"}
local ARGV = {"white","player5","black",100,10}

-- If you need to use some other DB uncomment next line and change number
-- redis.call("SELECT", 1)

dump(f(KEYS, ARGV))