local redis = require 'redis'
local dumpLib = require 'dump'

local dump = function(v) 
    if nil ~= v then
        print(dumpLib.tostring(v))
    end
end

-- If you have some different host/port change it here
local host = "127.0.0.1"
local port = 6379

client = redis.connect(host, port)

-- Workaround for absence of redis.call or atelast I did not find one
-- And did not want to digg in redis source code to see how does he get redis.call
redis.call = function(cmd, ...) 
    return assert(loadstring('return client:'.. string.lower(cmd) ..'(...)'))(...)
end

local f = function (KEYS, ARGV)
        local defaultratings=1000        
        local userscore=redis.call("ZSCORE",KEYS[1].."_scoreboard",ARGV[1])
        userscore=(true and userscore or defaultratings)
        redis.call("ZADD",KEYS[1]..ARGV[2],userscore,ARGV[1])
end
--          if (ARGV[nameCount] == "win" or ARGV[nameCount] == "draw" or ARGV[nameCount] == "lose" ) then            

-- Populate your ARGV and KEYS variables
--local ARGV = {"win:player1,player2","draw:player4","lose:player3"}
local KEYS = {"chess"}
local ARGV = {"player5","black"}

-- If you need to use some other DB uncomment next line and change number
-- redis.call("SELECT", 1)

dump(f(KEYS, ARGV))