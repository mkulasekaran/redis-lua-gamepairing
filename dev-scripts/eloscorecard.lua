local redis = require 'redis'
local dumpLib = require 'dump'

local dump = function(v) 
    if nil ~= v then
        print(dumpLib.tostring(v))
    end
end

-- If you have some different host/port change it here
local host = "127.0.0.1"
local port = 6379

client = redis.connect(host, port)

-- Workaround for absence of redis.call or atelast I did not find one
-- And did not want to digg in redis source code to see how does he get redis.call
redis.call = function(cmd, ...) 
    return assert(loadstring('return client:'.. string.lower(cmd) ..'(...)'))(...)
end

local f = function (KEYS, ARGV)
      
--        local K=32
        local K = ARGV[#ARGV]
        local win = {}
        local lose = {}
        local draw = {}
        local count=0
        local total=0
        local scoreTable={}
        local scoreboard=KEYS[1].."_scoreboard"

        local function split(s, delimiter)
            local result = {};
            for match in (s..delimiter):gmatch("(.-)"..delimiter) do
                table.insert(result, match);
            end
            return result;
        end
          
        local function startswith(String,Start)
                return string.sub(String,1,string.len(Start))==Start
        end  
         
        local function elo(Avgvalue,Value)
            return (1/(1+math.pow(10,((Avgvalue-Value)/400))))
        end
      
        local function round(num) 
          if num >= 0 then return math.floor(num+.5) 
          else return math.ceil(num-.5) end
        end
        
        

        for nameCount = 1, #ARGV do
          if ( startswith(ARGV[nameCount],"win")) then
            local Var=split(ARGV[nameCount], ":")
            win=split(Var[2],",")
          elseif ( startswith(ARGV[nameCount],"draw")) then
            local Var=split(ARGV[nameCount], ":")
            draw=split(Var[2],",")
          elseif ( startswith(ARGV[nameCount],"lose")) then
            local Var=split(ARGV[nameCount], ":")
            lose=split(Var[2],",")    
          end
        end  
       
        for nameCount = 1, #win do
              scoreTable[win[nameCount]]=redis.call("ZSCORE", scoreboard, win[nameCount])
              total=total+tonumber(scoreTable[win[nameCount]])
              count=count+1
        end
        for nameCount = 1, #draw do
              scoreTable[draw[nameCount]]=redis.call("ZSCORE", scoreboard, draw[nameCount])
              total=total+tonumber(scoreTable[draw[nameCount]])
              count=count+1
        end
        for nameCount = 1, #lose do
              scoreTable[lose[nameCount]]=redis.call("ZSCORE", scoreboard, lose[nameCount])
              total=total+tonumber(scoreTable[lose[nameCount]])
              count=count+1
        end
          
        local avg=total/count

        for nameCount = 1, #win do
            local Zscore=scoreTable[win[nameCount]]
            local Ratingscore=round(K*(1-elo(avg,Zscore)))
            redis.call("ZINCRBY", scoreboard,Ratingscore,win[nameCount])
        end
          
        for nameCount = 1, #draw do
            local Zscore=scoreTable[draw[nameCount]]
            local Ratingscore=round(K*(0.5-elo(avg,Zscore)))
            redis.call("ZINCRBY", scoreboard,Ratingscore,draw[nameCount])
        end         
           
        for nameCount = 1, #lose do
            local Zscore=scoreTable[lose[nameCount]]
            local Ratingscore=round(K*(0-elo(avg,Zscore)))
            redis.call("ZINCRBY", scoreboard,Ratingscore,lose[nameCount])
        end 
          
   -- return nil
end
--          if (ARGV[nameCount] == "win" or ARGV[nameCount] == "draw" or ARGV[nameCount] == "lose" ) then            

-- Populate your ARGV and KEYS variables
local ARGV = {"win:player1","lose:player2","16"}
local KEYS = {"blackjack"}

-- If you need to use some other DB uncomment next line and change number
-- redis.call("SELECT", 1)

dump(f(KEYS, ARGV))