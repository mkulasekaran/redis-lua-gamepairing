local redis = require 'redis'
local dumpLib = require 'dump'

local dump = function(v) 
    if nil ~= v then
        print(dumpLib.tostring(v))
    end
end

-- If you have some different host/port change it here
local host = "127.0.0.1"
local port = 6379

client = redis.connect(host, port)

-- Workaround for absence of redis.call or atelast I did not find one
-- And did not want to digg in redis source code to see how does he get redis.call
redis.call = function(cmd, ...) 
    return assert(loadstring('return client:'.. string.lower(cmd) ..'(...)'))(...)
end

local f = function (KEYS, ARGV)
      
--        local K=32
        local K = ARGV[#ARGV]
        local count=0
        local total=0
        local scoreTable={}
        local scoreboard=KEYS[1].."_scoreboard"
 
        local function elo(Avgvalue,Value)
            return (1/(1+math.pow(10,((Avgvalue-Value)/400))))
        end
      
        local function round(num) 
          if num >= 0 then return math.floor(num+.5) 
          else return math.ceil(num-.5) end
        end
                
        for nameCount = 1, #ARGV-1 do
              scoreTable[ARGV[nameCount]]=redis.call("ZSCORE", scoreboard, ARGV[nameCount])
              total=total+tonumber(scoreTable[ARGV[nameCount]])
              count=count+1
        end
         
        local avg=total/count
        count=count-1
        for nameCount = 1, #ARGV-1 do
            local Zscore=scoreTable[ARGV[nameCount]]
            local countValue=(1-((nameCount-1)/count))
            local Ratingscore=round(K*(countValue-elo(avg,Zscore)))
            redis.call("ZINCRBY", scoreboard,Ratingscore,ARGV[nameCount])
        end
          
   -- return nil
end
--          if (ARGV[nameCount] == "win" or ARGV[nameCount] == "draw" or ARGV[nameCount] == "lose" ) then            

-- Populate your ARGV and KEYS variables
local ARGV = {"player5","player6","player2","player7","player4","16"}
local KEYS = {"blackjack"}

-- If you need to use some other DB uncomment next line and change number
-- redis.call("SELECT", 1)

dump(f(KEYS, ARGV))