local keyLookup=KEYS[1]..ARGV[3]

local size=redis.call("ZCARD",keyLookup)

if (size < 1) then
  return "No Match available"
end  

local userlookup=KEYS[1]..ARGV[1]
local result={}
local rangeValue={}
local defaultratings=1000
local userscore=redis.call("ZSCORE",KEYS[1].."_scoreboard",ARGV[2])

userscore=true and userscore or defaultratings

for counter = 1,ARGV[5] do
    local interval=counter*ARGV[4]
    rangeValue=redis.call("ZRANGEBYSCORE",keyLookup,userscore-interval,userscore+interval)
    if table.getn(rangeValue) > 0  then 
      break 
    end
end

if (table.getn(rangeValue) == 0) then 
    rangeValue=redis.call("ZRANGEBYSCORE",keyLookup,"-inf","+inf")
    if (table.getn(rangeValue)==0) then
      return  "No Match available"
    end  
end  

local tablesize=table.getn(rangeValue)
local userFound=rangeValue[math.ceil(tablesize/2)]

redis.call("ZREM",keyLookup,userFound)
redis.call("ZREM",userlookup,ARGV[2])
table.insert(result, userFound) 
table.insert(result, ARGV[2])  

return result

