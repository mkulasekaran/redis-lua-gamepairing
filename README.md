Gameroom pairing lua scripts to be used for pairing opponents of equal strength.

**How it works?**
Each of the player who wins a particular match will have his rating increased based on the opponent elo ranking.

For more info on eloranking
http://en.wikipedia.org/wiki/Elo_rating_system

# REDIS LUA SCRIPTS  with the SHA functions in 64 bit ubuntu#

* createUser=fe5edc895c7b48c119b10ed37ce02e4b62e3d746
* eloranking=f44d0ad61c4ea2995a0ab7297d8d454cf7bf18af
* eloscoreboard=3ebeb7376655de7e3f94e85efcd811b5be83dc33
* matchmaking=f752af196eafb37f586086ac7a96e9dc34a4dbe2
* removeUser=e0c6e5472f44785af7ea48166a891bb1c7178328
* userIdle=d67ca1781e2270ed666e54bad1952c47ffcf2550
* removeIdle=f7d5d48d001b35b53c26e34ea72fd20d56b0eaed
* circularmatchmaking=b2007053c865c2ef172215b1e76cde9aa1f2b864
* userupdatematchmaking=491df89cae1117bd1e356ffac5ee288c871a5d72
* circularadduser=f9700ce75cd9aacceac212be9abfcbeb21feaf33


## SCRIPT FUNCTIONS ##

**K-value denotes the game status higher value more change**

### createUser ###
evalsha <sha1> <number of games> <<list of games>> <playername>

### removeUser ###
evalsha <sha1> <number of games> <<list of games>> <playername>

### userIdle ###
evalsha <sha1> 1 <gamename> <playername>

### removeIdle ###
evalsha <sha1> <number of games> <<list of games>> <playername>

### eloranking ###
evalsha <sha1> 1 <gamename> <<list of players ordered by their success>> <K-value>

### eloscoreboard ###
evalsha <sha1> 1 <gamename> (optional)<win:"list of players(csv)"> (optional)<draw:"list of players(csv)"> (optional)<lose:"list of players(csv)"> <K-value>

### matchmaking ###
evalsha <sha1> 1 <gamename> <playername> <required number of players> <add player to idle state(true/false)>

### circularmatchmaking ###
evalsha <sha1> 1 <gamename> <player color> <playername> <opponent color> <Rating interval> <Rating loop control>

### userupdatematchmaking ###
evalsha <sha1> 1 <gamename> <playername> <required number of players> <add player to idle state(true/false)> <<List of players to remove>>

### circularadduser ###
evalsha <sha1> 1 <gamename> <playername> <player color>